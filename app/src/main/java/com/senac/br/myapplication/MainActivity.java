package com.senac.br.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView txtDisplay;
    private EditText numero1;
    private EditText numero2;
    private Button btnSoma;
    private Button btnSubitracao;
    private Button btnDivisao;
    private Button btnMultiplicacao;
    private  double resultado = 0;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        txtDisplay = (TextView) findViewById(R.id.Display);


        numero1 = (EditText) findViewById(R.id.N1);
        numero2 =(EditText) findViewById(R.id.N2);

        btnSoma = (Button) findViewById(R.id.btnsoma);
        btnSubitracao = (Button) findViewById(R.id.btnsubitracao);
        btnMultiplicacao = (Button) findViewById(R.id.btnmultiplicacao);
        btnDivisao = (Button) findViewById(R.id.btndivisao);



        btnSoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double Operando1 = Double.parseDouble(numero1.getText().toString() );
                double Operando2 = Double.parseDouble(numero2.getText().toString() );
                double resultado = Operando1 + Operando2 ;

                txtDisplay.setText(String.valueOf(resultado));

            }
        });
        btnSubitracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double Operando1 = Double.parseDouble(numero1.getText().toString() );
                double Operando2 = Double.parseDouble(numero2.getText().toString() );
                double resultado = Operando1 - Operando2 ;

                txtDisplay.setText(String.valueOf(resultado));
            }
        });
        btnMultiplicacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double Operando1 = Double.parseDouble(numero1.getText().toString() );
                double Operando2 = Double.parseDouble(numero2.getText().toString() );
                double resultado = Operando1 * Operando2 ;

                txtDisplay.setText(String.valueOf(resultado));
            }
        });
        btnDivisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double Operando1 = Double.parseDouble(numero1.getText().toString() );
                double Operando2 = Double.parseDouble(numero2.getText().toString() );
                double resultado = Operando1 / Operando2 ;

                txtDisplay.setText(String.valueOf(resultado));
            }
        });
}
}
